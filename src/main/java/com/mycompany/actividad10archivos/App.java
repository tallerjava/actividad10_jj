package com.mycompany.actividad10archivos;

import java.io.IOException;

public class App {

    public static void main(String[] args) throws IOException {
        try {

            if (args == null || args.length != 1) {
                throw new IllegalArgumentException("Se requiere el nombre del archivo como parametro.");
            }
            DonacionRepository repositorio = new DonacionRepository(args[0]);
            DonacionService donacionService = new DonacionService(repositorio);
            Reporte reporte = donacionService.obtenerDonacionesPorPais();
            reporte.ordenarPorPrecioAscendentemente();
            System.out.format("%-45s %8s %n", "PAÍS", "DONACIÓN");
            for (Donacion donacion : reporte.getDonacionesPorPais()) {

                System.out.format("%-45s $%8.2f %n", donacion.getPais(), donacion.getMonto());
            }
            System.out.println();
            System.out.println("Total General: " + reporte.getTotalGeneral());

        } catch (IllegalArgumentException e) {
            e.getMessage();
        }
    }
}
