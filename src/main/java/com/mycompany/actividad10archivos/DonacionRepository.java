package com.mycompany.actividad10archivos;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class DonacionRepository {

    private final String nombreArchivo;
    private final List<Donacion> donaciones = new ArrayList<>();

    public DonacionRepository(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public List<Donacion> obtenerInformacion() throws IOException {

        try (BufferedReader bufferredReader = new BufferedReader(new InputStreamReader(new FileInputStream(nombreArchivo), "UTF-8"))) {
            String linea = bufferredReader.readLine();
            while ((linea = bufferredReader.readLine()) != null) {
                String[] cortarString = linea.split("\\|"); // 
                double valor = Double.parseDouble(cortarString[6].replace("$", ""));
                Donacion donacion = new Donacion(cortarString[1], valor);
                donacion.setPais(cortarString[1]);
                donacion.setMonto(valor);
                donaciones.add(donacion);
            }
        }
        return donaciones;
    }
}
