package com.mycompany.actividad10archivos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Reporte {

    private final List<Donacion> donacionesPorPais = new ArrayList();
    private double totalGeneral;

    Reporte(HashMap<String, Double> informacion, double totalGeneral) {
        this.totalGeneral = totalGeneral;
        for (Map.Entry<String, Double> donacion : informacion.entrySet()) {
            donacionesPorPais.add(new Donacion(donacion.getKey(), donacion.getValue()));
        }
        this.totalGeneral = totalGeneral;
    }

    public double getTotalGeneral() {
        return totalGeneral;
    }

    public List<Donacion> getDonacionesPorPais() {
        return donacionesPorPais;
    }

    public void ordenarPorPrecioAscendentemente() {
        Collections.sort(donacionesPorPais);
    }

    public void ordenarPorPrecioDescendentemente() {
        Collections.sort(donacionesPorPais, (Donacion donacion1, Donacion donacion2) -> donacion1.compareTo(donacion2));
    }
}
