package com.mycompany.actividad10archivos;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

public class DonacionService {

    DonacionRepository repositorio;

    DonacionService(DonacionRepository repositorio) {
        this.repositorio = repositorio;
    }

    public Reporte obtenerDonacionesPorPais() throws IOException {

        List<Donacion> donaciones = repositorio.obtenerInformacion();

        HashMap<String, Double> informacion = new HashMap();
        BigDecimal monto1, monto2, totalGeneralRecaudado = BigDecimal.ZERO;
        double monto = 0;
        for (Donacion donacion : donaciones) {

            if (informacion.containsKey(donacion.getPais())) {
                monto1 = new BigDecimal(donacion.getMonto());
                monto2 = new BigDecimal(informacion.get(donacion.getPais()));
                monto1 = BigDecimal.valueOf(monto1.doubleValue()).add(BigDecimal.valueOf(monto2.doubleValue()));

                informacion.put(donacion.getPais(), monto1.doubleValue());
            } else {
                informacion.put(donacion.getPais(), donacion.getMonto());
            }
            totalGeneralRecaudado = totalGeneralRecaudado.add(new BigDecimal(donacion.getMonto()));
            monto = totalGeneralRecaudado.doubleValue();
        }

        return new Reporte(informacion, monto);
    }

}
