package com.mycompany.actividad10archivos;

public class Donacion implements Comparable<Donacion> {

    private String pais;
    private double monto;

    public Donacion(String pais, double monto) {
        this.pais = pais;
        this.monto = monto;
    }

    public String getPais() {
        return pais;
    }

    public double getMonto() {
        return monto;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    @Override
    public int compareTo(Donacion donacion) {
        if (this.monto < donacion.monto) {
            return -1;
        } else if (this.monto > donacion.monto) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        Donacion other = (Donacion) obj;

        if (pais == null) {
            if (other.pais != null) {
                return false;
            }
        } else if (!pais.equals(other.pais)) {
            return false;
        }
        return Double.compare(monto, other.monto) == 0;
    }
}
