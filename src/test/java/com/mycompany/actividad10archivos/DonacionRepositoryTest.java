package com.mycompany.actividad10archivos;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;

public class DonacionRepositoryTest {

    @Test
    public void obtenerInformacion_archivoValido_listadoDeDonacionesPorPais() throws IOException {

        String nombreArchivo = "donacionesTest.txt";
        DonacionRepository instance = new DonacionRepository(nombreArchivo);
        List<Donacion> resultadoEsperado = new ArrayList<>();

        resultadoEsperado.add(new Donacion("Bouvet", Double.parseDouble("1.20")));
        resultadoEsperado.add(new Donacion("Swaziland", Double.parseDouble("3.42")));
        resultadoEsperado.add(new Donacion("Suriname", Double.parseDouble("3.77")));
        resultadoEsperado.add(new Donacion("Mauritius", Double.parseDouble("4.44")));
        resultadoEsperado.add(new Donacion("Sri Lanka", Double.parseDouble("1.68")));
        resultadoEsperado.add(new Donacion("Tanzania", Double.parseDouble("2.40")));
        resultadoEsperado.add(new Donacion("Palestine", Double.parseDouble("4.77")));
        resultadoEsperado.add(new Donacion("Suriname", Double.parseDouble("2.11")));
        resultadoEsperado.add(new Donacion("Thailand", Double.parseDouble("0.57")));
        resultadoEsperado.add(new Donacion("Suriname", Double.parseDouble("3.17")));

        List<Donacion> resultadoObtenido = instance.obtenerInformacion();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test(expected = FileNotFoundException.class)
    public void obtenerInformacion_nombreDelArchivoIncorrecto_() throws IOException {

        String nombreArchivo = "dona.txt";
        DonacionRepository instance = new DonacionRepository(nombreArchivo);
        List<Donacion> resultadoObtenido = instance.obtenerInformacion();

    }
}
