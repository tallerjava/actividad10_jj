package com.mycompany.actividad10archivos;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class DonacionServiceTest {

    @Test
    public void obtenerDonacionesPorPais_listadoDeDonacionesPorPaisValido_reporte() throws Exception {
        HashMap donaciones = new HashMap();

        donaciones.put("Bouvet", Double.parseDouble("1.20"));
        donaciones.put("Swaziland", Double.parseDouble("3.42"));
        donaciones.put("Suriname", Double.parseDouble("9.05"));
        donaciones.put("Mauritius", Double.parseDouble("4.44"));
        donaciones.put("Sri Lanka", Double.parseDouble("1.68"));
        donaciones.put("Tanzania", Double.parseDouble("2.40"));
        donaciones.put("Palestine", Double.parseDouble("4.77"));
        donaciones.put("Thailand", Double.parseDouble("0.57"));

        double totalGeneral = 27.53f;
        Reporte resultadoEsperado = new Reporte(donaciones, totalGeneral);
        String nombreArchivo = "donacionesTest.txt";
        DonacionRepository repositorio = new DonacionRepository(nombreArchivo);
        DonacionService donacionService = new DonacionService(repositorio);
        Reporte resultadoObtenido = donacionService.obtenerDonacionesPorPais();
        assertEquals(resultadoEsperado.getDonacionesPorPais(), resultadoObtenido.getDonacionesPorPais());
        assertEquals(resultadoEsperado.getTotalGeneral(), resultadoObtenido.getTotalGeneral(), 0.009);
    }

    @Test(expected = FileNotFoundException.class)
    public void obtenerDonacionesPorPais_nombreDelArchivoIncorrecto_() throws IOException {

        String nombreArchivo = "dona.txt";
        DonacionRepository repositorio = new DonacionRepository(nombreArchivo);
        DonacionService donacionService = new DonacionService(repositorio);
        Reporte resultadoObtenido = donacionService.obtenerDonacionesPorPais();

    }
}
