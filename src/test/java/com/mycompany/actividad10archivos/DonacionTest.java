package com.mycompany.actividad10archivos;

import org.junit.Test;
import static org.junit.Assert.*;

public class DonacionTest {

    @Test
    public void compareTo_objetoEsMenorAlQueSeCompara_menosUno() {
        Donacion donacion1 = new Donacion("Argentiva", 123.4);
        Donacion donacion2 = new Donacion("Brasil", 321.45);
        assertTrue(donacion1.compareTo(donacion2) == -1);
    }

    @Test
    public void compareTo_objetoEsIgualAlQueSeCompara_cero() {
        Donacion donacion1 = new Donacion("Argentiva", 123.4);
        Donacion donacion2 = new Donacion("Brasil", 123.4);
        assertTrue(donacion1.compareTo(donacion2) == 0);
    }

    @Test
    public void compareTo_objetoEsMayorAlQueSeCompara_uno() {
        Donacion donacion1 = new Donacion("Argentiva", 321.4);
        Donacion donacion2 = new Donacion("Brasil", 123.45);
        assertTrue(donacion1.compareTo(donacion2) == 1);
    }

    @Test
    public void equals_objetosDiferentes_retornaFalse() {
        Donacion donacion1 = new Donacion("Argentiva", 123.4);
        Donacion donacion2 = new Donacion("Brasil", 321.45);
        assertFalse(donacion1.equals(donacion2));
    }

    @Test
    public void equals_objetosIguales_retornaTrue() {
        Donacion donacion1 = new Donacion("Argentiva", 123.4);
        Donacion donacion2 = new Donacion("Argentiva", 123.4);
        assertTrue(donacion1.equals(donacion2));
    }

    @Test
    public void equals_paisesIgualesMontosDiferentes_retornaFalse() {
        Donacion donacion1 = new Donacion("Argentiva", 123.4);
        Donacion donacion2 = new Donacion("Argentiva", 321.45);
        assertFalse(donacion1.equals(donacion2));
    }

    @Test
    public void equals_paisesDiferentesMontosIguales_retornaFalse() {
        Donacion donacion1 = new Donacion("Argentiva", 123.4);
        Donacion donacion2 = new Donacion("Brasil", 123.4);
        assertFalse(donacion1.equals(donacion2));
    }

}
